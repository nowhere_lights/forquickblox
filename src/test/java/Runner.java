import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Runner {

    private static WebDriver driver;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "g://java_projects/QuickbloxTestTask/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        final String BASE_URL = "http://quickblox.com/";
        driver.get(BASE_URL);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testForQuickBlox() throws InterruptedException {
        WebElement documentationDropDown = driver.findElement(By.id("menu-item-97"));
        documentationDropDown.click();

        WebElement restApiButton = driver.findElement(By.xpath(".//*[@id='mw-content-text']/div[2]/div[1]/a/span"));
        restApiButton.click();

        WebElement yourAccountDetails = driver.findElement(By.xpath(".//*[@id='mw-content-text']/div[1]/p/a/img"));
        boolean imagePresent = yourAccountDetails.isDisplayed();
//        String path = yourAccountDetails.getAttribute("src");
//        System.out.println(path);
        if (!imagePresent) {
            System.out.printf("No image");
        } else {
            System.out.printf("Image appeared");
        }

        Thread.sleep(5000);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
